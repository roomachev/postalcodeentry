package com.example.rsahi.postalcodeentry;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.InputFilter;
import android.text.InputType;
import android.text.Spanned;
import android.text.method.KeyListener;
import android.view.KeyEvent;
import android.view.View;
import android.widget.EditText;

import com.example.rsahi.postalcodeentry.text.BaseKeyListener;

/**

 Develop a text field with an on-screen keyboard for entering Canadian postal codes. The specification is as follows:

 Develop the app as a Java Android 4.1 solution. Target a screen size of 320x480 (to simulate a ruggedized mobile device).
 The user should not be able to enter an invalid postal code, with validation occurring character by character.
 For instance, when the user is at the point in the field where a number must be entered, the keyboard should only enable the numeric keys.
 A valid postal code has the following characteristics:
 Format: letter-number-letter-number-letter-number (i.e. M4B1G5)
 The letters can only be one of the following: A, B, C, E, G, H, J, K, L, M, N, P, R, S, T, V, X, Y
 The on-screen keyboard should include a backspace key.
 Speed of keyboard reconfiguration as the user enters the postal code is key. Assume it will run on an older ruggedized device, so the processor and RAM are limited

 */
public class PostalCodeEntryActivity extends AppCompatActivity {

    protected EditText mPostalEditText;

    protected KeyListener mLetterKeyListener  = new PostalLetterKeyListener();
    protected KeyListener mNumberKeyListener = new PostalNumberKeyListener();

    protected InputFilter[] mLettersFilters = new InputFilter[] {new PostalLetterInputFilter()};
    protected InputFilter[] mNumbersFilters = new InputFilter[] {new PostalNumberInputFilter()};

    protected boolean mHandledDelete;
    protected String mLastValidInput = "";


    protected enum EntryState {
        UNKNOWN,
        LETTER,
        NUMBER
    }

    /**
     *     KeyListeners Handle Delete and Keyboard layout changes by
     *     catching the backspace key (del)
     */
    protected class PostalLetterKeyListener extends BaseKeyListener {
        @Override
        public int getInputType() {
            return InputType.TYPE_TEXT_FLAG_CAP_CHARACTERS;
        }
        @Override
        public boolean onKeyDown(View view, Editable text, int keyCode, KeyEvent event) {
            if( event.getKeyCode() == KeyEvent.KEYCODE_DEL ) {
                handleDelete(text);
            }
            return false;
        }
    }

    protected class PostalNumberKeyListener extends BaseKeyListener {
        @Override
        public int getInputType() {
            return InputType.TYPE_CLASS_NUMBER;
        }
        @Override
        public boolean onKeyDown(View view, Editable text, int keyCode, KeyEvent event) {
            if(event.getKeyCode() == KeyEvent.KEYCODE_DEL) {
                handleDelete(text);
            }
            return false;
        }
    }

    protected void handleDelete(Editable text) {

        int length = text.length();
        if (length > 1) {
            // have to update mLastValidInput before modifiying "text" because it invokes the
            // input filters when you modify the Editable.
            // Also cannot reuse the text.delete(length - 1, length) expression
            // because toString crashes with those same bounds as are on the editable
            mHandledDelete = true;
            String mTempStr = text.toString();
            mLastValidInput = mTempStr.substring(0,mTempStr.length()-1);
            text = text.delete(length - 1, length);
        } else if (length == 1){
            mHandledDelete = true;
            mLastValidInput = "";
            text.clear();
        } else if (length == 0) {
            // you are not deleting, you are just clearing the input buffer
        }
    }



    /**
     *     InputFilters to ensure the postal format is enforced midstream
     */
    protected class PostalLetterInputFilter extends BasePostalInputFilter {

        @Override
        protected String getRegex() {
            return "[ABCEGHJKLMNPRSTVXY]";
        }

        @Override
        protected EntryState getNextState() {
            return EntryState.NUMBER;
        }
    }

    protected class PostalNumberInputFilter extends BasePostalInputFilter {

        @Override
        protected String getRegex() {
            return "[0-9]";
        }

        @Override
        protected EntryState getNextState() {
            return EntryState.LETTER;
        }
    }

    protected abstract class BasePostalInputFilter implements InputFilter {

        protected final int MAX_LENGTH = 6;
        protected String mSourceString = "";

        @Override
        public CharSequence filter(CharSequence source, int start, int end, Spanned dest, int dstart, int dend) {

            mSourceString = source.toString();

            int destLength = dest.length();
            boolean isSourceEmpty = "".equalsIgnoreCase(mSourceString);
            boolean isDestEmpty = destLength > 0;

            if (isSourceEmpty && mHandledDelete) {
                mHandledDelete = false;
                switchToState(getNextState());
                return null;
            } else if(destLength > (MAX_LENGTH-1)) {
                // too many over limit attempts empties the field
                return "";
            } else if (mSourceString.matches(getRegex())) {
                mLastValidInput += mSourceString;
                switchToState(getNextState());
                return null;
            } else if (isDestEmpty){
                return mLastValidInput;
            } else {
                return "";
            }
        }

        protected abstract String getRegex();
        protected abstract EntryState getNextState();
    }

    protected void switchToState(EntryState newState) {

        if (newState == EntryState.LETTER) {

            mPostalEditText.setKeyListener(mLetterKeyListener);
            mPostalEditText.setFilters(mLettersFilters);

        } else if (newState == EntryState.NUMBER) {

            mPostalEditText.setKeyListener(mNumberKeyListener);
            mPostalEditText.setFilters(mNumbersFilters);
        }
    }


    /**
     * The main entry point for the 'screen'
     *
     * @param savedInstanceState
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_postal_code_entry);

        mPostalEditText = (EditText)findViewById(R.id.edit_postal);
        mPostalEditText.setFilters(new InputFilter[]{new PostalLetterInputFilter()});

    }

}
